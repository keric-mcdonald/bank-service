package com.bank.repository;

import org.springframework.data.repository.CrudRepository;

import com.bank.dataobjects.Account;

public interface AccountRepository extends CrudRepository<Account, Integer>  {

}
