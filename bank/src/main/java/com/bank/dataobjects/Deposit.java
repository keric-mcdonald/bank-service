package com.bank.dataobjects;

public class Deposit {
	private int id;
	private double deposit;
	public Deposit() {
		super();
	}
	public Deposit(int id, double deposit) {
		super();
		this.id = id;
		this.deposit = deposit;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getDeposit() {
		return deposit;
	}
	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	

}
