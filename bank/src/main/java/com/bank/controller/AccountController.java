package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dataobjects.Account;
import com.bank.dataobjects.Deposit;
import com.bank.dataobjects.Transfer;
import com.bank.service.AccountService;

@CrossOrigin
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/Accounts")
	public List<Account> getUsers() {
		return accountService.getAccounts();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Accounts")
	public void addUser(@RequestBody Account account) {
		accountService.addUpdateAccount(account);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/Accounts/Deposit")
	public Account deposit(@RequestBody Deposit deposit) {
		Account account = accountService.getAccount(deposit.getId());
		account.setAccountBalance(account.getAccountBalance() + deposit.getDeposit());
		accountService.addUpdateAccount(account);
		return accountService.getAccount(deposit.getId());
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/Accounts/withdraw")
	public Account withdraw(@RequestBody Deposit deposit) {
		Account account = accountService.getAccount(deposit.getId());
		if (account.getAccountBalance() >= deposit.getDeposit())
			account.setAccountBalance(account.getAccountBalance() - deposit.getDeposit());
		accountService.addUpdateAccount(account);
		return accountService.getAccount(deposit.getId());
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/Accounts/transfer")
	public List<Account> transfer(@RequestBody Transfer transfer) {
		Account from = accountService.getAccount(transfer.getFrom());
		Account to = accountService.getAccount(transfer.getTo());
		
		from.setAccountBalance(from.getAccountBalance() - transfer.getAmount());
		accountService.addUpdateAccount(from);
		
		to.setAccountBalance(to.getAccountBalance() + transfer.getAmount());
		accountService.addUpdateAccount(to);
		
		return accountService.getAccounts();
	}

	@RequestMapping("/Accounts/{id}")
	public Account getAccount(@PathVariable String id) {
		return accountService.getAccount(Integer.parseInt(id));
	}

}
