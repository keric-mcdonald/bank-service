package com.bank.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dataobjects.Account;
import com.bank.repository.AccountRepository;

@Service
public class AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	
	public void addUpdateAccount(Account account) {
		accountRepository.save(account);
	}
	public List<Account> getAccounts() {			
		List<Account> accounts = new ArrayList<Account>();
		accountRepository.findAll().forEach(accounts::add);
		return accounts;		
	}
	public Account getAccount(int id) {		
		return accountRepository.findById(id).get();
	}

}
