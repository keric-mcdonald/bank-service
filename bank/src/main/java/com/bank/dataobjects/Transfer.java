package com.bank.dataobjects;

public class Transfer {
	private int from;
	private int to;
	private Double amount;
	public Transfer() {
		super();
	}
	public Transfer(int from, int to, Double amount) {
		super();
		this.from = from;
		this.to = to;
		this.amount = amount;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	

}
